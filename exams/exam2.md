# Exam 2 - Build your own Docker image
Using acloudguru platform complete the tasks listed below.


**Prerequisities:**
- personal access to hub.docker.com
- access to gitlab.com

<br>

**ToDo:**
Build your own Docker Image using provided template and store it into Docker hub.

**1.** Ensure that you can access (via your personalized account)
- hub.dockerhub.com
- gitlab.com 

**2.** Prepare your "developer's" machine 
- separate VM with some Linux distro is needed, you will use it for interacting with Docker
- Install Docker engine on your machine

**3.** Prepare your own code using provided template
- Template: https://github.com/ritaly/HTML-CSS-CV-demo
- Adjust Name, Picture, supply your real data

**4.** Buid your own Docker image from your code which will serve you online web app `mycv`
- Docker Image Name: `mycv:1.0.0`
- Base image to use: `nginx:stable`

_HINT:_ you will need to create Docker file and build Image from it

**5.** Test your doings by creating Docker Container from your Image on your VM
- Container Name: `mycv`
- Port to be exposed: `80`

**6.** Push your Image into Dockerhub registry

`<YOUR_USER>/mycv:1.0.0`

**7.** Create Repository on `gitlab.com` under your personalized account
- Repository Name: mycv

**8.** Save your app code into your `gitlab.com` repository project `mycv`

**9.** Final check

