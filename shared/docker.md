# Docker Tutorial

[[_TOC_]]

![K8S Architecture](shared/img/docker.png)



## General
 - **Docker:** Container Runtime + Tool for managing containers and image
 - **Containerd:** Container Runtime
 - [**Crictl:**](https://kubernetes.io/docs/tasks/debug/debug-cluster/crictl/) CLI for CRI-compatible Container runtimes
 - **Podman:** Tool for managing containers and images

## Install Docker

```
## Ubuntu
# Install package
apt install docker.io

# Check 
systemctl status docker
```

## Check / download image
```
# check local images
docker images

# search 
docker search image_name

# pull image
docker pull image_name e.g. docker pull hello-world
```

<br>

## Run containers
The easiest way how to test if everything is OK, just run container from `hello-world` image
```
docker run hello-world
```
NOTE: if hello-world image is missing on your local machine it will be downloaded first. So it means that `docker run` will call `docker pull`.

```
# Check containers
docker ps 
docker ps -a
```
<br>

## Build own Docker image
1. Prepare `Dockerfile`

Here is sample example of Dockerfile
```
FROM nginx:latest

COPY . /usr/share/nginx/html/

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
```
NOTE:
- `FROM` specifies base image to use
- `COPY` is self-explanatory
- `EXPOSE` on which port should app by exposed
- `CMD` which process will be started inside container

Another example of Dockerfile:
```
FROM bash

CMD ["ping", "www.google.sk"]
```


2. Build Image from provided `Dockerfile`
```
docker build -t petyb/pbarczi-cv:v1 .
```
<br>

## Push Image into Image registry
```
# Login to container registry
docker login
NOTE: you'll be asked to provide your Docker hub credentials

# Push Image
docker push petyb/pbarczi-cv:v1
```

<br>

## Create container 
```
docker run -d --name mycvcontainer1 -p 8001:80 petyb/pbarczi-cv:v1
```
NOTES:
- `-d` means that container will run in aka `detached mode`
- via `--name` you can name you container, otherwise random generated name will be used
- with `-p` you can redirect ports e.g. you can access port `8001` on your host, but it will redirect you inside container's port `80` 
- proper image name `username/image_name:tag`
- `--rm` also removes container when it's stopped

<br>

## Remove Container
```
docker rm mycvcontainer1
```
NOTE: you can also use container's ID instead of it's name.


<br>


## Docker logs
```
docker logs mycvcontainer1
```


## Remove Image
```
docker rmi image_name
docker rmi image_id
```

<br>


## Commands

Display only Container IDs
```
docker ps --format '{{.ID}}'
```

<br>

# Useful links
- https://medium.com/nttlabs/the-internals-and-the-latest-trends-of-container-runtimes-2023-22aa111d7a93
