# AWS

[[_TOC_]]



## Install awscli

```
sudo apt udpate
sudo apt install awscli
```

## Configure awscli
```
aws configure
```

## AWS Commands

### Buckets 
```
# List Buckets
aws s3 ls --region us-east-1

# Create Buckets
aws s3api create-bucket --bucket petybucket --region us-east-1

# Copy local file to a Buckets
aws s3 --region us-east-1 cp test s3://petybucket
```

### EC2
```
# List Regions
aws ec2 describe-regions --output table

# Security Group
aws ec2 create-security-group --group-name demo --description "Demo"
NOTE: ID

# Security Group Ingress Rule
aws ec2 authorize-security-group-ingress --group-name demo --protocol tcp --port 22 --cidr 0.0.0.0/0

# Key pair
aws ec2 create-key-pair --key-name demo-key --query 'KeyMaterial' --output text > demo.pem
chmod 600 demo.pem

# Create EC2 (AmazonLinux)
aws ec2 run-instances --image-id ami-0f9fc25dd2506cf6d --security-group-ids xxxxxxxxx --instance-type t3.micro --key-name demo-key

# Get its Public IP
aws ec2 describe-instances --instance-ids i-0787e4282810ef9cf --query 'Reservations[0].Instances[0].PublicIpAddress'

# SSH to VM
ssh -i demo.pem ec2-user@<PUBLIC_IP>

# Delete instance
aws ec2 terminate-instances --instance-ids i-0787e4282810ef9cf
```
<br>

