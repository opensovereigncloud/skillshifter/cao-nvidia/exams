# Useful Commands

[[_TOC_]]

## 

### Docker
For commands for interacting with Docker refer [here.](shared/docker.md)

### Kubernetes
In order to dislable tls verification you can adjust your kubeconfig like this:
```
- cluster:
    insecure-skip-tls-verify: true
    server: https://<IP>:6443
  name: test
```

