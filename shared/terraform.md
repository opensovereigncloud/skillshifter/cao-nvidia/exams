# Terraform

[[_TOC_]]



## Install terraform
For installation steps refer [here](https://developer.hashicorp.com/terraform/install)

```
wget https://releases.hashicorp.com/terraform/1.7.2/terraform_1.7.2_linux_amd64.zip
unzip terraform_1.7.2_linux_amd64.zip
mv terraform /usr/local/bin

terraform -h
```


## Terraform Commands

### Terraform Init
Following the official [docu.](https://registry.terraform.io/providers/hashicorp/aws/latest/docs)

Create `provider.tf`
```
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}
```

```
terraform init
```
### Create resource
```
resource "aws_vpc" "demo" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = false
}
```


### Terraform Plan
```
terraform apply
terraform apply --auto-approve
```

### Terraform Apply
```
terraform apply
```

### Terraform State
```
terraform state list
```

### Terraform Destroy
```
terraform destroy
terraform destroy --auto-approve
```

<br>
