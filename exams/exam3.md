# Exam 3 - Build CICD Pipeline using gitlab
Using acloudguru platform complete the tasks listed below.


**Prerequisities:**
- personal access to hub.docker.com
- access to gitlab.com
- gitlab repo with your source code of your web app (mycv)

<br>

**ToDo:**

Build CICD pipeline using `gitlab.com` in order to automatically build new version of Docker Image for your app and push it into the Docker hub.

**1.** Ensure that you can access (via your personalized account)
- hub.dockerhub.com
- gitlab.com 

**2.** Prepare `.gitlab-ci.yaml` as follows:
- 1 Stage called `BUILD_STAGE`
- 2 Jobs under `BUILD_STAGE`
  - `build_job` which builds Docker image 
  - assign tag `latest` to your Image
  - `push_job` which pushes Docker image into Docker registry

**3.** Test

Test your new Image byt creating a temporary Docker container using that new Image.
