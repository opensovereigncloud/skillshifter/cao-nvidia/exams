# Exam 4 - Deploy K8S test environment using CICD Pipeline and perform Kubernetes tasks
Using acloudguru platform complete the tasks listed below.


**Prerequisities:**
- access to gitlab.com
- K8S knowledge from Training

<br>

**ToDo:**

**1. Deploy K8s cluster**

Deploy your Kubernetes test cluster using your CICD pipeline (k3s or kubeadm) and complete there the Tasks from CKA Training.

<br>

**2. Complete the Tasks listed below:**

```
1) Create three namespaces

    -) production
    -) development
    -) testing ( capacity: 36 pods, hint: ResourceQuota )
 
2) How many namespaces are there in your cluster ?

3) Label your workers
    -) worker01(perf=gold,comp=gpu,dc=ba,rack=08)
    -) worker02(perf=silver,comp=cpu,dc=ke,rack=09)
    -) worker03(perf=bronze,comp=cpu,dc=ke,rack=15)

4) List all workers that
    -) have label perf=gold
    -) have label comp=cpu
    -) don't have label perf=gold

5) Label namespaces
    -) production(important=yes)
    -) development(important=no)
    -) testing(important=yes)

6) Annotate namespaces
    -) production(gopas.sk/admin=Fero)
    -) development(gopas.sk/admin=Jurko)
    -) testing(gopas.sk/admin=Peter)

7) Create POD "webserver" in each namespace ( ie. production, testing and development )
    -) container name: proxy
    -) image: support01.k8s.lab/bitnami/nginx
    -) NginX must listen on port 9090/tcp ( environment variable NGINX_HTTP_PORT_NUMBER )
    -) must be scheduled only on nodes in dc=ke

8) Create DEPLOYMENT "mycv" in production namespace
    -) container name: mycv
    -) replicas: 3
    -) image: <YOUR-MYCV-IMAGE>

9) Expose previously created DEPLOYMENT as SERVICE "mycv" in production namespace
    -) service name: mycv
    -) image: <YOUR-MYCV-IMAGE>
    -) service type: NodePort
    -) exposed port: 80  
    -) node port: 30080
```

