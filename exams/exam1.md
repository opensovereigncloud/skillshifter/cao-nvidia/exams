# Exam 1 - Deploy Kubernetes cluster (k3s)
Using acloudguru platform complete the tasks listed below.


**Prerequisities:**
- acloudguru credentials
- gitlab.com credentials

<br>

**ToDo:**

**1.** Create Kubernetes cluster according to given specification:
- Number of Master/Control plane nodes: 1
- Number of Worker nodes: 2
- Distro: k3s v1.29.0+k3s1

**2.** Create sample deployment as follows:
- Deployment name: test
- Namespace: skillshifter
- Image: nginx:latest
- Replicas: 10

**3.** Access your cluster using [LENS](https://k8slens.dev/)

**4.** Final check

- on master node create user `admin` with the following ssh-key:

`ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB+FcJU11IIvmuF+YGQTCjjshbAVHWGrbsrovA6bp9C5 pb`

<br>

**Help:**
- https://k3s.io/
- https://www.scaleway.com/en/docs/tutorials/deploy-k3s-cluster-with-cilium/

<br>

**Solution**
