# Reskill CAO/NVIDIA

[[_TOC_]]

## Technology stack
- Linux
- Containers (Docker)
- Kubernetes
- Git
- Gitlab CICD
- Terraform ?
- Public cloud (AWS, Azure,...)

<br>

## Exams
### [Exam 1 - Deploy Kubernetes cluster](exams/exam1.md) 
### [Exam 2 - Build your own Docker Image](exams/exam2.md)
### [Exam 3 - Build CICD pipeline](exams/exam3.md)
### [Exam 4 - Deploy K8s cluster and complete Tasks](exams/exam4.md) 
### [Exam 5 - Automate app depployment using GitOps tool Flux CD](exam/exam5.md) 

<br>

## Commands
You can find usefull commands [here.](commands.md)

<br>

## Study materials
- [Nie prilis structy uvod do Kubernetes](https://www.youtube.com/watch?v=uvNQgsJLnvE&t=2536s)
- [Gitlab CICD](https://www.youtube.com/watch?v=Jav4vbUrqII)   
- [Git uvod](https://www.youtube.com/watch?v=9Mbo3Av6cxw&list=PLNAMH_0HgWT-ey31hQqrmi_Rgr4OVWgH3)
- [Upgrade cluster](https://scriptcrunch.com/upgrade-kubernetes-kubeadm-cluster/)

 
