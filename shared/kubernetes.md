# Kubernetes Tutorial
Imperative vs. declarative configuration

[[_TOC_]]

## Architecture
![K8S Architecture](shared/img/k8s-arch-2.png)

<br>

## Install Kubernetes
For K3S installation refer [here.](https://docs.k3s.io/quick-start)

**Hints**
```
# Aliases
alias k=kubectl
```

<br>

## Cluster info
```
# Cluster info
kubectl cluster-info

# Nodes
kubectl get no
kubectl get no -o yaml
```

<br>

## Building blocks of K8s
- NAMESPACE
- POD
- DEPLOYMENT
- SERVICE

<br>

### NAMESPACE
```
kubectl get ns 
kubectl create ns rocky
```

<br>

### POD
POD is basic build block of Kubernetes.

**Create POD**
```
kubectl run rocky --image=petyb/rocky:1 -n rocky
```

**View POD**
```
kubectl get po rocky -n rocky -o wide
kubectl get po -A 
```

**Inspect POD**
```
kubectl describe po rocky
```

**Remove POD**
```
kubectl delete po rocky -n rocky
```

**Access POD**
```
kubectl exec -it rocky -n rocky -- /bin/bash
```

**POD's logs**
```
kubectl logs pod rocky -n rocky
```

<br>

### DEPLOYMENT
**Create Deployment**
```
kubectl create deploy -n rocky rocky --image=petyb/rocky:1 --replicas=3
```

**Create Deployment**
```
kubectl delete deploy -n rocky rocky
```

**Adjust Image**
```
kubectl set image deployment/rocky rocky=petyb/rocky:2 -n rocky
```

OR

```
kubectl edit deployment/rocky -n rocky
```

**Scale Deployment**
```
kubectl scale deployment rocky --replicas=9 -n rocky
```
<br>


### SERVICE
**Create Service**
```
kubectl expose deployment rocky -n rocky --type=NodePort --port=80 --target-port=80

kubectl get svc
```

**Access Service**
```
http://PUBLICIP:NODEPORT
```

**Delete Service**
```
kubectl delete svc rocky -n rocky
```

<br>

# Useful links
- https://kubernetes.io
- https://k8slens.dev
