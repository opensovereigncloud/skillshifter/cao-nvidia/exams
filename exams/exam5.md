# Automated app deployment via FluxCD

### FluxCD
The goal of this Exam is to get familiar with FLUX CD.

[Flux](https://fluxcd.io/) is a GitOps operator for Kubernetes that works by synchronizing the state of manifests in a Git repository to the designated setting for a cluster.

- created by Weaveworks
- CNCF incubating project
- runs in Kubernetes cluster
- comes with a set of Custom Resource Definitions
- has a CLI 
- consists of the following controllers:
  - helm-controller
  - image-automation-controller
  - image-reflector-controller
  - kustomize-controller
  - notification-controller
  - source-controller

<br></br>

<br></br>

### In order to complete this LAB follow these Steps:


### 0. Prerequisities
- access to Kubernetes existing cluster

<br>

### 1. Gain a personal access to gitlab.com
Bring your own gitlab.com's account or create new one [here.](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial&_gl=1*i69kfn*_ga*MjQ3ODgyMDkuMTY2NDk3MzUyNA..*_ga_ENFH3X7M5Y*MTY2NDk3MzUyNC4xLjAuMTY2NDk3MzUyNy4wLjAuMA..) 

<br></br>
### 2. Create Personal Access Token for your GitLab account
GitLab > Account > Preferences > Access Tokens > Generate new one
> NOTE: select All Scopes

<br></br>
### 3. Access your cluster
You need the access to existing Kubernetes cluster in order to complete next Tasks
- access cluster via `kubectl` is sufficient
- don't need an SSH access


<br></br>
### 4. Check 
In order to continue your cluster needs to be in READY state. Let's check it:
```
kubectl get no
```

<br></br>
### 5. Flux CD Installation 
- Install fluxctl CLI
```
curl -s https://fluxcd.io/install.sh | sudo bash

# bash autocompletion
. <(flux completion bash)
```
- Verify
```
flux --help
```
- check prerequisities
```
flux check
flux check --pre
```

<br></br>
### 6. Export GitLab Variables
```
export GITLAB_USER="Your gitlab.com username"
export GITLAB_TOKEN="Your Personal Access Token"
```

<br></br>
### 7. Bootstrap Flux CD
```
flux --help 

flux bootstrap gitlab \
  --owner=$GITLAB_USER \
  --repository=flux-sample \
  --branch=main \
  --path=./clusters/my-cluster \
  --personal \
  --token-auth
```
> NOTE: repository name you specify will be created via Flux automatically in case that it doesn't exist yet

<br></br>
### 8. Check Flux's installation
```
kubectl get all -n flux-system
```

<br></br>
### 9. Deploy webapp
- Here is a sample webapp code  which can be used for demo: https://gitlab.com/pety.barczi/rocky

- Let's deploy app:
  - under your flux-sample repository create directory structure like this named flux-example/clusters/my-cluster/my-app1
  - store application related manifests into the `my-app1` folder
```
flux-sample/
└── clusters
    └── my-cluster
        ├── flux-system
        │   ├── gotk-components.yaml
        │   ├── gotk-sync.yaml
        │   └── kustomization.yaml
        └── my-app1
            ├── namespace.yaml
            ├── deployment.yaml
            ├── service.yaml
            └── kustomization.yaml

```
>NOTE: you can also use your CV app image.

  - Wait for Flux to synchronize with GitLab

<br>

### 10. Access your webapp
`https://<YOUR-PUBLIC-IP>`